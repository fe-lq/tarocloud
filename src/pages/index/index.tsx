import React, { useState, useEffect } from 'react'
import Taro from '@tarojs/taro'
import { View, Image, Text, Input, Button, EventProps, Checkbox, Label, CheckboxGroup } from '@tarojs/components'
import './index.scss'
import { InputProps } from '@tarojs/components/types/Input'
import { CheckboxGroupProps } from '@tarojs/components/types/CheckboxGroup'
import Api from '../../api'
export default () => {
  useEffect(() => {
    // 调用云函数
    Api.login().then((res: any) => {
      console.log('[云函数] [login] user openid: ', res.result.openid)
    })
    getTodo()
  },
  []);
  const [list, setList] = useState<{
    text: string,
    flag: boolean,
    _id: string
  }[]>([])
  const [text, setText] = useState('')
  const [imagePath, setImagePath] = useState('')
  const [isShow, setIsShow] = useState<boolean>(false)
  const handleInput: InputProps['onInput'] = (e) => {
    setText(e.detail.value)
  }
  // 请求添加
  const setToBase = (text: string) => {
    Api.add({ text }).then((res: any) => {
      if (res.errMsg.includes('ok')) {
        getTodo()
        setText('')
        Taro.showToast({
          title: '添加成功',
          icon: 'none',
          duration: 1000
        })
      }
    })
  }
  // 请求查询
  const getTodo = () => {
    Api.query().then((res: any) => {
      if (res.errMsg.includes('ok')) {
        setList(res.result.data)
      }
    })
  }
  // 添加
  const handleAdd: EventProps['onClick'] = () => {
    if (text) {
      setToBase(text)
      // setText('')
    } else {
      Taro.showToast({
        title: '请输入添加的内容',
        icon: 'none',
        duration: 2000
      })
    }
  }
  const handleConfig: EventProps['onClick'] = () => {
    setIsShow(!isShow)
  }
  // 修改状态
  const changeStatus: CheckboxGroupProps['onChange'] = (e) => {
    Api.update({ ids: e.detail.value }).then((res: any) => {
      if (res.errMsg.includes('ok')) {
        getTodo()
      }
    })
  }
  // 删除
  const handleDel: EventProps['onClick'] = (e) => {
    // 请求删除
    Api.remove({ id: e.target.dataset.id }).then((res: any) => {
      if (res.errMsg.includes('ok')) {
        getTodo()
        Taro.showToast({
          title: '删除成功',
          icon: 'none',
          duration: 1000
        })
      }
    })
  }

  // 上传文件
  const chooseImage: EventProps['onClick'] = () => {
    Taro.chooseImage({
      count: 5,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: (res) => {
        console.log('文件上传成功', res)
        const filePath = res.tempFilePaths[0]
        const img = filePath.match(/.[^.]+?$/) || []
        const cloudPath = `${Date.now()}-${Math.floor(Math.random() * 1000)}` + img[0]
        Taro.cloud.uploadFile({
          cloudPath,
          filePath,
          success: (res) => {
            console.log(res)
            setImagePath(res.fileID)
          }
        })
      }
    })
  }

  return (
    <View className='content'>
      <Input className='input' value={text} onInput={handleInput}></Input>
      <View className='btn-groups'>
        <Button className='btn' size='mini' type="primary" onClick={handleAdd}>添加</Button>
        {list.length > 0 && <Button className='btn' size='mini' type={isShow?'primary':'default'} onClick={handleConfig}>管理</Button>}
      </View>
      <View className='list'>
        <CheckboxGroup onChange={changeStatus}>
          {list.map(item => {
            return (
              <View className='list-item' key={item._id}>
                <Label className={item.flag ? 'complete' : ''}>
                  <Checkbox value={item._id} checked={item.flag} />
                  <Text>{item.text}</Text>
                </Label>
                {isShow && <Text className='del' data-id={item._id} onClick={handleDel} style='color: red'>删除</Text>}
              </View>
            )
          })}
        </CheckboxGroup>
      </View>
      <Button onClick={chooseImage}>上传图片</Button>
      <Image src={imagePath}></Image>
    </View>
  )
}
