const cloud = require("wx-server-sdk");

// 初始化 cloud
cloud.init({
  // API 调用都保持和云函数当前所在环境一致
  env: cloud.DYNAMIC_CURRENT_ENV
})

exports.main = async ({ text }) => {
  try {
    const { OPENID } = cloud.getWXContext();
    console.log(OPENID)
    const db = cloud.database();
    // 新增数据到数据库的counters表中
    return await db.collection('counters').add({
      data: {
        '_openid': OPENID,
        flag: false,
        text
      },
    })
  } catch (error) {
    console.log(error)
  }
};
