const cloud = require("wx-server-sdk");

// 初始化 cloud
cloud.init({
  // API 调用都保持和云函数当前所在环境一致
  env: cloud.DYNAMIC_CURRENT_ENV
})

exports.main = async (event) => {
  const { OPENID } = cloud.getWXContext();
  console.log(OPENID)
  const db = cloud.database();
  // 查询当前用户所有的 counters
  return await db.collection('counters').where({
    _openid: OPENID
  }).get()
};
