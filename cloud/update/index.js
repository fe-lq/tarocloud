const cloud = require("wx-server-sdk");

// 初始化 cloud
cloud.init({
  // API 调用都保持和云函数当前所在环境一致
  env: cloud.DYNAMIC_CURRENT_ENV
})
exports.main = async ({ ids }) => {
  const { OPENID } = cloud.getWXContext();
  console.log(OPENID)
  const db = cloud.database();
  const _ = db.command
  // 查询当前用户所有的 counters
  db.collection('counters').where({
    _openid: OPENID,
    _id: _.nin(ids)
  }).update({
    data: {
      flag: false
    }
  })
  return await db.collection('counters').where({
    _openid: OPENID,
    _id: _.in(ids)
  }).update({
    data: {
      flag: true
    }
  })
};
